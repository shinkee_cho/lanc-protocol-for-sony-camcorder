
/*************************************************************
 * Special Topic
 * Controls sony Camcorder(Video Camera) by LANC protocol
 * 
 * by Sing (Shinkee Cho)
 * Student ID : 10013041
 *  
 * Reference 
 * http://controlyourcamera.blogspot.com/2011/02/arduino-controlled-video-recording-over.html
 *************************************************************/

/*************************************************************

"LANC" is a registered trademark of SONY.
CANON calls their LANC compatible port "REMOTE".

Sub-Command

Power on : Connect LANC Signal to GND for >140 ms to power on

BYTE 0 :
0001 1000  Normal command to VTR or video camera
0010 1000 Special command to video camera
0011 1000 Special command to VTR
0001 1110 Normal command to still video camera0001 1000  Normal command to VTR or video camera
* "Low-Nibble": Device Code
* "High-Nibble": Guide Code

BYTE 1 :
* Btye 0 is '0010 1000' Special command to video camera

00  variable speed zoom Tele: slowest speed
02  variable speed zoom Tele: faster than 00
04  variable speed zoom Tele: faster than 02
06  variable speed zoom Tele: faster than 04
08  variable speed zoom Tele: faster than 06
0A  variable speed zoom Tele: faster than 08
0C  variable speed zoom Tele: faster than 0A
0E  variable speed zoom Tele: fastest speed
10  variable speed zoom Wide: slowest speed
12  variable speed zoom Wide: faster than 10
14  variable speed zoom Wide: faster than 12
16  variable speed zoom Wide: faster than 14
18  variable speed zoom Wide: faster than 16
1A  variable speed zoom Wide: faster than 18
1C  variable speed zoom Wide: faster than 1A
1E  variable speed zoom Wide: fastest speed
21  grid (AVCHD)
25  fader
27  rec start (DV, some cameras)
29  rec stop (DV, some cameras)
30  variable speed zoom Tele (avoiding digital zoom, some cameras): slowest speed
32  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 30
34  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 32
35  Zoom Tele slow (working all cameras since approx. 1996)
36  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 34
37  Zoom Wide slow (working all cameras since approx. 1996)
38  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 36
39  Zoom Tele fast (working all cameras since approx. 1996)
3A  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 38
3B  Zoom Wide fast (working all cameras since approx. 1996)
3C  variable speed zoom Tele (avoiding digital zoom, some cameras): faster than 3A
3E  variable speed zoom Tele (avoiding digital zoom, some cameras): fastest speed
41  Auto-Focus on/off (not if there is a real switch at the camera)
45  Focus manual far
47  Focus manual near
49  White balance toggle (only cameras until approx. 1996)
4B  Backlight (not DV)
51  Backlight (DV)
53  Exposure auto/man. toggle (models of the early 90's)
    Iris more close
55  Iris more open
61  Shutter (models of the early 90's)
77  White balance reset (not if white balance is selected via menu)
85  Memory impose (models of the early 90's)
87  Color / Mode (models of the early 90's)
89  Superimpose (models of the early 90's)
AF  Iris auto
*/

#define cmdPin 12 
#define lancPin 11
#define recButton 13
#define zoomOutButton 3
#define zoomInButton 2
#define focusNearButton 5
#define focusFarButton 4
#define powerOffButton 10

int cmdRepeatCount;
int bitDuration = 104; //Duration of one LANC bit in microseconds. 


//LANC commands byte 0 + byte 1
//Tested with Canon XF300

//Start-stop video recording
boolean REC[] = {LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW,   LOW,LOW,HIGH,HIGH,LOW,LOW,HIGH,HIGH}; //18 33

//Zoom in from slowest to fastest speed
boolean ZOOM_IN_0[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,LOW,LOW,LOW,LOW}; //28 00
boolean ZOOM_IN_1[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,LOW,LOW,HIGH,LOW}; //28 02
boolean ZOOM_IN_2[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,LOW,HIGH,LOW,LOW}; //28 04
boolean ZOOM_IN_3[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,LOW,HIGH,HIGH,LOW}; //28 06
boolean ZOOM_IN_4[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,HIGH,LOW,LOW,LOW}; //28 08
boolean ZOOM_IN_5[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,HIGH,LOW,HIGH,LOW}; //28 0A
boolean ZOOM_IN_6[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,HIGH,HIGH,LOW,LOW}; //28 0C
boolean ZOOM_IN_7[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,LOW,HIGH,HIGH,HIGH,LOW}; //28 0E

//Zoom out from slowest to fastest speed
boolean ZOOM_OUT_0[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,LOW,LOW,LOW,LOW}; //28 10
boolean ZOOM_OUT_1[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,LOW,LOW,HIGH,LOW}; //28 12
boolean ZOOM_OUT_2[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,LOW,HIGH,LOW,LOW}; //28 14
boolean ZOOM_OUT_3[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,LOW,HIGH,HIGH,LOW}; //28 16
boolean ZOOM_OUT_4[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW}; //28 18
boolean ZOOM_OUT_5[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,HIGH,LOW,HIGH,LOW}; //28 1A
boolean ZOOM_OUT_6[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,HIGH,HIGH,LOW,LOW}; //28 1C
boolean ZOOM_OUT_7[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,LOW,LOW,HIGH,HIGH,HIGH,HIGH,LOW}; //28 1E

//Focus control. Camera must be switched to manual focus
boolean FOCUS_NEAR[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,HIGH,LOW,LOW,LOW,HIGH,HIGH,HIGH}; //28 47
boolean FOCUS_FAR[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,HIGH,LOW,LOW,LOW,HIGH,LOW,HIGH}; //28 45

boolean FOCUS_AUTO[] = {LOW,LOW,HIGH,LOW,HIGH,LOW,LOW,LOW,   LOW,HIGH,LOW,LOW,LOW,LOW,LOW,HIGH}; //28 41

boolean POWER_OFF[] = {LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW,   LOW,HIGH,LOW,HIGH,HIGH,HIGH,HIGH,LOW}; //18 5E
//boolean POWER_ON[] = {LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW,   LOW,HIGH,LOW,HIGH,HIGH,HIGH,LOW,LOW}; //18 5C  Doesn't work because there's no power supply from the LANC port when the camera is off
//boolean POWER_OFF2[] = {LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW,   LOW,LOW,HIGH,LOW,HIGH,LOW,HIGH,LOW}; //18 2A Turns the XF300 off and then on again
//boolean POWER_SAVE[] = {LOW,LOW,LOW,HIGH,HIGH,LOW,LOW,LOW,   LOW,HIGH,HIGH,LOW,HIGH,HIGH,LOW,LOW}; //18 6C Didn't work

boolean isOff = false;

void setup() {

  pinMode(lancPin, INPUT);       // listens to the LANC line
  pinMode(cmdPin, OUTPUT);       // writes to the LANC line
  
  pinMode(recButton, INPUT);     // start-stop recording button
  digitalWrite(recButton, HIGH); // turn on an internal pull up resistor
  
  pinMode(zoomOutButton, INPUT); 
  digitalWrite(zoomOutButton, HIGH);
  
  pinMode(zoomInButton, INPUT); 
  digitalWrite(zoomInButton, HIGH);
  
  pinMode(focusNearButton, INPUT); 
  digitalWrite(focusNearButton, HIGH);
  
  pinMode(focusFarButton, INPUT); 
  digitalWrite(focusFarButton, HIGH);
  
  pinMode(powerOffButton, INPUT); 
  digitalWrite(powerOffButton, HIGH);
   
  digitalWrite(cmdPin, LOW);     // set LANC line to +5V
  delay(5000);                   // Wait for camera to power up completly

  bitDuration = bitDuration - 8; // Writing to the digital port takes about 8 microseconds so only 96 microseconds are left for each bit
}

void loop() {

  // Record
  if (!digitalRead(recButton))
  {
    lancCommand(REC); 
  }

  // Zoom out
  if (!digitalRead(zoomOutButton))
  {
    lancCommand(ZOOM_OUT_4); 
  }

  // Zoom In
  if (!digitalRead(zoomInButton))
  {
    lancCommand(ZOOM_IN_4); 
  }

  // Focus near
  if (!digitalRead(focusNearButton))
  {
    lancCommand(FOCUS_NEAR); 
  }

  // Focus Far
  if (!digitalRead(focusFarButton))
  {
    lancCommand(FOCUS_FAR); 
  }

  // Power off
  if (!digitalRead(powerOffButton))
  {
    sendByte0(POWER_OFF);
    sendByte1(POWER_OFF);
  }
}



void lancCommand(boolean lancBit[]) {
       
  cmdRepeatCount = 0;
  
  while (cmdRepeatCount < 5) {  //repeat 5 times to make sure the camera accepts the command


// BYTE 0 :
  sendByte0(lancBit);

//    while (pulseIn(lancPin, HIGH) < 5000) {   
//      //"pulseIn, HIGH" catches any 0V TO +5V TRANSITION and waits until the LANC line goes back to 0V 
//      //"pulseIn" also returns the pulse duration so we can check if the previous +5V duration was long enough (>5ms) to be the pause before a new 8 byte data packet
//      //Loop till pulse duration is >5ms
//    }
//  
//    //LOW after long pause means the START bit of Byte 0 is here
//    delayMicroseconds(bitDuration);  //wait START bit duration
//    
//    //Write the 8 bits of byte 0 
//    //Note that the command bits have to be put out in reverse order with the least significant, right-most bit (bit 0) first
//    for (int i=7; i>-1; i--) {
//      digitalWrite(cmdPin, lancBit[i]);  //Write bits. 
//      delayMicroseconds(bitDuration); 
//    }
//
//    //Byte 0 is written now put LANC line back to +5V
//    digitalWrite(cmdPin, LOW);
//    delayMicroseconds(10); //make sure to be in the stop bit before byte 1
//
//    while (digitalRead(lancPin)) { 
//      //Loop as long as the LANC line is +5V during the stop bit
//    }



  // BYTE 1 : 
  sendByte1(lancBit);
  
//    //0V after the previous stop bit means the START bit of Byte 1 is here
//    delayMicroseconds(bitDuration);  //wait START bit duration
//    
//    //Write the 8 bits of Byte 1
//    //Note that the command bits have to be put out in reverse order with the least significant, right-most bit (bit 0) first
//    for (int i=15; i>7; i--) {
//      digitalWrite(cmdPin,lancBit[i]);  //Write bits 
//      delayMicroseconds(bitDuration);
//    }
//  
//    //Byte 1 is written now put LANC line back to +5V
//    digitalWrite(cmdPin, LOW); 
//    
//    cmdRepeatCount++;  //increase repeat count by 1
    
    /*Control bytes 0 and 1 are written, now don’t care what happens in Bytes 2 to 7
    and just wait for the next start bit after a long pause to send the first two command bytes again.*/
  
  } // While cmdRepeatCount < 5
}

void sendByte0(boolean lancBit[])
{
    while (pulseIn(lancPin, HIGH) < 5000)
    {   
      //"pulseIn, HIGH" catches any 0V TO +5V TRANSITION and waits until the LANC line goes back to 0V 
      //"pulseIn" also returns the pulse duration so we can check if the previous +5V duration was long enough (>5ms) to be the pause before a new 8 byte data packet
      //Loop till pulse duration is >5ms
    }
  
    //LOW after long pause means the START bit of Byte 0 is here
    delayMicroseconds(bitDuration);  //wait START bit duration
    
    //Write the 8 bits of byte 0 
    //Note that the command bits have to be put out in reverse order with the least significant, right-most bit (bit 0) first
    for (int i=7; i>-1; i--)
    {
      digitalWrite(cmdPin, lancBit[i]);  //Write bits. 
      delayMicroseconds(bitDuration); 
    }

    //Byte 0 is written now put LANC line back to +5V
    digitalWrite(cmdPin, LOW);
    delayMicroseconds(10); //make sure to be in the stop bit before byte 1

    while (digitalRead(lancPin))
    { 
      //Loop as long as the LANC line is +5V during the stop bit
    }
}

void sendByte1(boolean lancBit[])
{
    //0V after the previous stop bit means the START bit of Byte 1 is here
    delayMicroseconds(bitDuration);  //wait START bit duration
    
    //Write the 8 bits of Byte 1
    //Note that the command bits have to be put out in reverse order with the least significant, right-most bit (bit 0) first
    for (int i=15; i>7; i--) {
      digitalWrite(cmdPin,lancBit[i]);  //Write bits 
      delayMicroseconds(bitDuration);
    }
  
    //Byte 1 is written now put LANC line back to +5V
    digitalWrite(cmdPin, LOW); 
    
    cmdRepeatCount++;  //increase repeat count by 1
}
